package threads;

public class Main {
    public static void main(String[] args) {
        double[] t = new double[200000000];
        Filler f = new Filler();
        long t0;
        t0 = System.currentTimeMillis();
        f.fill(t);
        System.out.println("no thread\t" + (System.currentTimeMillis() - t0));
        for (int nbt = 1; nbt < 20; nbt++) {
            FastFiller ff = new FastFiller(nbt);
            t0 = System.currentTimeMillis();
            ff.fill(t);
            System.out.println(nbt + "\t" + (System.currentTimeMillis() - t0));
        }
    }
}
