package threads;

public class Filler {
    public void fill(double[] t) {
        for (int i = 0; i < t.length; i++) {
            t[i] = Math.sin(i);
        }
    }
}
